const fs = require("fs");
const path = require("path");
const util = require("util");
const { promisify } = util;

let _path = path.resolve(__dirname);
_path = _path.substring(0, _path.length - 5);
const localeDir = path.join(_path, "types");
(async () => {
  try {
    const readLocaleDir = await promisify(fs.readdir)(localeDir);
    const writePath = path.join(localeDir, "index.d.ts");
    // 备份内容
    let bakContent = await promisify(fs.readFile)(writePath, "utf8");
    // 清空内容
    await promisify(fs.writeFile)(writePath, "", "utf8");
    /*  readLocaleDir.forEach(async (l) => {
    }); */
    // console.log(readLocaleDir);
    for (let i = 0; i < readLocaleDir.length; i++) {
      if (
        readLocaleDir[i] !== "index.test.d.ts" &&
        readLocaleDir[i] !== "index.d.ts"
      ) {
        const filePath = path.join(localeDir, readLocaleDir[i]);
        const readFile = await promisify(fs.readFile)(filePath, "utf8");
        const result = readFile.replace(
          new RegExp(/export default (.+?);/g),
          function (match, value) {
            return "";
          }
        );
        fs.appendFileSync(writePath, result, "utf8");
      }
    }

    const writeRes = bakContent.replace(
      new RegExp(/import (.+?) from '(.+?)';/g),
      function (match, value) {
        return "";
      }
    );
    await promisify(fs.appendFile)(writePath, writeRes, "utf8");
    // 重新读取文件删除空行
    bakContent = await promisify(fs.readFile)(writePath, "utf8");
    const dataArry = bakContent.split("\n");
    const newArry = [];
    dataArry.forEach((item) => {
      if (item) {
        newArry.push(item);
      }
    });
    await promisify(fs.writeFile)(writePath, newArry.join("\n"), "utf8");
  } catch (e) {
    console.error(e);
  }
})();
