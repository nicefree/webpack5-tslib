const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const isProduction = process.env.NODE_ENV === "production";
const outputType = process.env.OUTPUT_TYPE; // 读取当前的输出格式（UMD/ESM）

const config = {
  mode: isProduction ? "production" : "development",
  entry:
    // 打包输出 ESM 格式文件，最终要输出多个文件，便于实现按需加载，因此设置为多入口。
    outputType === "esm"
      ? {
          add: "./src/calc/add.ts",
          subtract: "./src/calc/subtract.ts",
          multiply: "./src/calc/multiply.ts",
          divide: "./src/calc/divide.ts",
          test: "./src/calc/test.ts",
        }
      : isProduction
      ? "./src/calc/index.ts"
      : "./src/index.ts",
  // 由于输出 ESM 格式文件为 Webpack 实验特性，因此需要加上此配置。
  experiments: {
    outputModule: outputType === "esm",
  },
  // 针对不同的环境变量，执行不同的打包动作。
  output:
    outputType === "esm"
      ? // ESM
        {
          path: path.resolve(__dirname, "es"),
          filename: "[name].esm.js",
          library: {
            type: "module",
          },
          chunkFormat: "module",
          clean: true,
        }
      : // UMD
        {
          path: path.resolve(__dirname, "lib"),
          filename: "index.js",
          library: {
            name: "calc",
            type: "umd",
            export: "default",
          },
          globalObject: "globalThis",
          clean: true,
        },
  // 使路径查找时，支持省略文件名的 ts 后缀。
  resolve: {
    extensions: [".js", ".json", ".ts"],
  },
  // Babel 与 TS 配置
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [["@babel/preset-env"]],
            },
          },
          { loader: "ts-loader" },
        ],
      },
    ],
  },
  devServer: {
    hot: true, // 热更新
    port: 9999, // 开启端口号
    client: {
      //在浏览器端打印编译进度
      progress: true,
    },
  },
  // html-webpack-plugin 只需在开发环境时使用。
  plugins: [
    ...(!isProduction
      ? [
          new HtmlWebpackPlugin({
            template: "index.html",
            inject: "body",
            scriptLoading: "blocking",
          }),
        ]
      : []),
  ],
};

module.exports = config;
