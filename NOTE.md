### 创建 webpack5-tslib

npm init -y

安装相关插件 npm -i webpack webpack-cli html-webpack-plugin webpack-dev-server

### 创建 Webpack 配置文件

```js
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const config = {
  mode: "development",

  entry: "./src/index.ts",

  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    clean: true,
  },

  // 使路径查找时，支持省略文件名的 ts 后缀。
  resolve: {
    extensions: [".js", ".json", ".ts"],
  },
  // Babel 与 TS 配置
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [["@babel/preset-env"]],
            },
          },
          { loader: "ts-loader" },
        ],
      },
    ],
  },
  devServer: {
    hot: true, // 热更新
    port: 9999, // 开启端口号
    client: {
      //在浏览器端打印编译进度
      progress: true,
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "index.html",
      inject: "body",
      scriptLoading: "blocking",
    }),
  ],
};

module.exports = config;
```

### Babel 与 TS 配置

安装 babel-loader 与 ts-loader 相关依赖，支持 ES6+ 转 ES5 以及支持利用 TypeScript 语言开发工具库

npm i -D @babel/core @babel/preset-env babel-loader typescript ts-loader

其中， @babel/core 为 babel 的核心依赖模块，@babel/preset-env 为 babel 提供的预设插件。

@babel/preset-env 仅支持对 ES6+ 语法进行转换，但对于一些 ES6+ API 是无法转换的（例如 Promise、Async/Await 等），如果对新 API 的兼容性有需求，请参考 core-js、@babel/preset-typescript 相关用法即可

### 开发调试

创建 src/calc/add.ts

```ts
function add(...args: number[]) {
  return args.reduce((ac, cur) => ac + cur);
}

export default add;
```

创建 src/calc/divide.ts

```ts
function divide(...args: number[]) {
  return args.reduce((ac, cur) => ac / cur);
}

export default divide;
```

创建 src/calc/multiply.ts

```ts
function multiply(...args: number[]) {
  return args.reduce((ac, cur) => ac * cur);
}

export default multiply;
```

创建 src/calc/subtract.ts

```ts
function subtract(...args: number[]) {
  return args.reduce((ac, cur) => ac - cur);
}

export default subtract;
```

创建 src/calc/index.ts

```ts
import add from "./add";
import subtract from "./subtract";
import multiply from "./multiply";
import divide from "./divide";

const calc = {
  add,
  subtract,
  multiply,
  divide,
};

export default calc;
export { add, subtract, multiply, divide };
```

创建 src/index.ts

```ts
import calc from "./calc";
console.log(calc.add(1, 2));
console.log(calc.subtract(1, 2));
console.log(calc.multiply(1, 2));
console.log(calc.divide(1, 2));
```

修改 package.json

```json
{
  // 其他配置已省略
  "scripts": {
    "dev": "npx webpack serve",
    "build": "npx webpack"
  }
}
```

npm run dev 查看浏览器输出结果

### 实现自动化测试 (了解)

安装 Jest 相关依赖
npm i -D jest ts-jest @types/jest

初始化 Jest 配置文件
npx ts-jest config:init

编写测试用例 ./calc/index.test.ts

```ts
import { add, subtract, multiply, divide } from "./index";

test("add test", () => {
  expect(add(1, 2)).toBe(3);
});

test("subtract test", () => {
  expect(subtract(1, 2)).toBe(-1);
});

test("multiply test", () => {
  expect(multiply(1, 2)).toBe(2);
});

test("divide test", () => {
  expect(divide(1, 2)).toBe(0.5);
});
```

package.json 中添加测试 script

"test": "jest ./src/calc/index.test.ts"

执行测试 npm run test

### 打包

打包输出 UMD 格式文件

通过 cross-env 修改环境变量来实现区分生产/开发环境

npm i -D cross-env

修改 package.json script 字段配置

build": "cross-env NODE_ENV=production webpack"

修改 webpack 配置

```js
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const isProduction = process.env.NODE_ENV === "production";

const config = {
  mode: isProduction ? "production" : "development",
  entry: isProduction ? "./src/calc/index.ts" : "./src/index.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
    library: {
      name: "calc", // 指定库名称
      type: "umd", // 输出的模块化格式， umd 表示允许模块通过 CommonJS、AMD 或作为全局变量使用
      export: "default", // 指定将入口文件的默认导出作为库暴露
    },
    globalObject: "globalThis", // 设置全局对象为 globalThis，使库同时兼容 Node.js 与浏览器环境
    clean: true,
  },

  // 使路径查找时，支持省略文件名的 ts 后缀。
  resolve: {
    extensions: [".js", ".json", ".ts"],
  },
  // Babel 与 TS 配置
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [["@babel/preset-env"]],
            },
          },
          { loader: "ts-loader" },
        ],
      },
    ],
  },
  devServer: {
    hot: true, // 热更新
    port: 9999, // 开启端口号
    client: {
      //在浏览器端打印编译进度
      progress: true,
    },
  },
  // html-webpack-plugin 只需在开发环境时使用。
  plugins: [
    ...(!isProduction
      ? [
          new HtmlWebpackPlugin({
            template: "index.html",
            inject: "body",
            scriptLoading: "blocking",
          }),
        ]
      : []),
  ],
};

module.exports = config;
```

此时，生成的文件已支持通过 CommonJS、AMD、浏览器全局变量（window）引用等多种引入方式。同时，在 Webpack 环境下，也支持通过 ESM 方式来引入此文件。
但当前的打包方式会将所有函数打包在一起，不利于 ESM Tree Shaking，因此，将利用 Webpack5 支持输出 ESM 格式文件的特性，单独输出文件的 ESM 格式版本。

输出 ESM 格式文件

通过输出 ESM 格式文件，实现 ESM 方式引入下的模块函数按需加载。

Webpack5 通过指定 output.library.type 值为 module，来实现输出 ESM 格式文件。
通过设置自定义环境变量（OUTPUT_TYPE），将输出 ESM 格式文件作为一个单独的任务。

修改 package.json script 配置

```js
"build": "npm run test & npm run generate:esm & npm run generate:umd",
"generate:umd": "cross-env NODE_ENV=production OUTPUT_TYPE=umd webpack --config ./webpack.config.js",
"generate:esm": "cross-env NODE_ENV=production OUTPUT_TYPE=esm webpack --config ./webpack.config.js",
```

没必要 npm run test 可以去除

执行 npm run build
其中 es 文件夹下的产物支持 ESM 格式引入，支持按需加载。lib 文件夹下的产物支持 CommonJS、AMD 以及全局变量引入

### 引入方式

CommonJS 引入

const calc = require('webpack5-tslib')

浏览器 Script 标签引入

```js
<script src="webpack5-tslib/lib/index.js"></script>
<script>
  window.calc.add(1,2); // 结果为 3
</script>
```

ESM 引入

// 整体引入
import calc from 'webpack5-tslib'

// 按需加载引入
import add from 'webpack5-tslib/es/add.esm'

### 生成 TS 类型声明文件

配置生成 TS 类型声明文件，便于用户在使用库时进行相关的类型提示。
修改 TS 配置文件（tsconfig.json）

执行打包操作，生成类型声明文件

npm run build

修改 package.json 指定当前模块的入口文件、类型声明入口文件。

```js
{
  "main": "lib/index.js",
  "typings": "types/index.d.ts",
}
```

### 通过 CDN 访问自己发布的库

https://cdn.jsdelivr.net/npm/(your packagename)@(version)/(file)

### 其他

- 使用 Webpack 构建 JavaScript 工具库 ts 细节 https://blog.csdn.net/hwjfqr/article/details/126551922
- https://blog.csdn.net/Wustfish/article/details/132152632

### 调试

```ts
const files = require.context("../calc/", false, /\.ts/);

const calc: { [key: string]: any } = {};
files.keys().forEach((key) => {
  const fileName = key.replace(/(.*\/)*([^.]+).*/gi, "$2"); // 提取文件名
  if (fileName.toLowerCase() !== "index") {
    // 排除 index.ts
    const func = files(key);
    calc[fileName] = func.default;
    module.exports[fileName] = func.default;
  }
});
console.log(module.exports);
export default { ...module.exports, default: { ...module.exports } };
```

### 调试合并

```js
const fs = require("fs");
const path = require("path");
const util = require("util");
const { promisify } = util;

let _path = path.resolve(__dirname);
_path = _path.substring(0, _path.length - 5);
const localeDir = path.join(_path, "types");
(async () => {
  try {
    const readLocaleDir = await promisify(fs.readdir)(localeDir);
    const writePath = path.join(localeDir, "index.d.ts");
    // 备份内容
    let bakContent = await promisify(fs.readFile)(writePath, "utf8");
    // 清空内容
    await promisify(fs.writeFile)(writePath, "", "utf8");
    /*  readLocaleDir.forEach(async (l) => {
    }); */
    // console.log(readLocaleDir);
    for (let i = 0; i < readLocaleDir.length; i++) {
      if (
        readLocaleDir[i] !== "index.test.d.ts" &&
        readLocaleDir[i] !== "index.d.ts"
      ) {
        const filePath = path.join(localeDir, readLocaleDir[i]);
        const readFile = await promisify(fs.readFile)(filePath, "utf8");
        const result = readFile.replace(
          new RegExp(/export default (.+?);/g),
          function (match, value) {
            return "";
          }
        );
        fs.appendFileSync(writePath, result, "utf8");
      }
    }

    const writeRes = bakContent.replace(
      new RegExp(/import (.+?) from '(.+?)';/g),
      function (match, value) {
        return "";
      }
    );
    await promisify(fs.appendFile)(writePath, writeRes, "utf8");
    // 重新读取文件删除空行
    bakContent = await promisify(fs.readFile)(writePath, "utf8");
    const dataArry = bakContent.split("\n");
    const newArry = [];
    dataArry.forEach((item) => {
      if (item) {
        newArry.push(item);
      }
    });
    await promisify(fs.writeFile)(writePath, newArry.join("\n"), "utf8");
  } catch (e) {
    console.error(e);
  }
})();
```

- 2.修改 build 脚本

```js
"build": "npm run generate:esm & npm run generate:umd & node build/esm",
```

### 处理按需引入类型声明

https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/README.zh-Hans.md
