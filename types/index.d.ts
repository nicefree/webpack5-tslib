import add from './add';
import subtract from './subtract';
import multiply from './multiply';
import divide from './divide';
import test from './test';
declare const calc: {
    add: typeof add;
    subtract: typeof subtract;
    multiply: typeof multiply;
    divide: typeof divide;
    test: typeof test;
};
export default calc;
export { add, subtract, multiply, divide, test };
