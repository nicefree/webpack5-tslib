function getBase64Img(imgObj: HTMLImageElement) {
  var canvas = document.createElement("canvas");
  canvas.width = imgObj.width;
  canvas.height = imgObj.height;
  var ctx = canvas.getContext("2d");
  ctx?.drawImage(imgObj, 0, 0, imgObj.width, imgObj.height);
  var ext = imgObj.src.substring(imgObj.src.lastIndexOf(".") + 1).toLowerCase();
  var dataURL = canvas.toDataURL("image/" + ext);
  return dataURL;
}

function test(url: string): Promise<string> {
  return new Promise((resolve) => {
    const img = new Image();
    img.crossOrigin = "";
    img.src = url;
    img.onload = function () {
      resolve(getBase64Img(img));
    };
  });
}

export default test  