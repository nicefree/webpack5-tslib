import add from './add'
import subtract from './subtract'
import multiply from './multiply'
import divide from './divide'
import test from './test'

const calc = {
  add,
  subtract,
  multiply,
  divide,
  test
}

export default calc
export { add, subtract, multiply, divide, test }
 